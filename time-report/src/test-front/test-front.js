
import { LitElement, html } from 'lit';
import '../test-api/test-api.js';

class TestFront extends LitElement{

    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }


    render(){
        return html`
            <h1>Time Report Front</h1>
            <test-api @test-api-event="${this.pruebaEvento}"></test-api>
        `
    }

    pruebaEvento(ev) {
        console.log("pruebaEvento en test-front");
        e.preventDefault();
        console.log(ev);
    }
}

customElements.define("test-front",TestFront);