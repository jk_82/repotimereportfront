import { LitElement, html } from "lit";


class ReportListado extends LitElement{

    static get properties(){
        return{
            empleados: {type: Array},
            empleadosListados: {type: Array}
        };
    }

    constructor(){
        super();
        this.empleados = [];
        this.empleadosListados = [];
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
            <div>
            <button class="btn  btn-success float-right"
                style="margin-top: 10px; float: right; margin-bottom: -32px;"
                @click="${this.addEmpleado}">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                </svg>
            </button>
            </div>

            <table class="table table-hover table-bordered caption-top">
            <caption class="text-center">Lista de empleados</caption>
            <thead class="table-info">
                <th scope="col">Código usuario</th>
                <th scope="col">Nombre y Apellidos</th>
                <th scope="col">Tarea</th>
                <th scope="col">Horas</th>
                <th scope="col">Editar/borrar</th>
            </thead>
            <tbody>
               ${this.empleadosListados.map(
                   empleado => html`
                    <tr>
                        <td>${empleado.coduser}</td>
                        <td>${empleado.nombre}</td>
                        <td>${empleado.tarea}</td>
                        <td>${empleado.horas}</td>
                        <td>
                        <button @click="${ev => this.deleteEmpleados(ev, empleado.id)}"
                            class="btn col-3"
                            style="background-color: transparent; border-color: transparent;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">
                                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </button>
                        <button @click="${ev => this.infoEmpleado(ev, empleado)}"
                            class="btn col-3"
                            style="background-color: transparent; border-color: transparent;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                            </svg>
                        </button>
                        </td>
                    </tr>
                    `
               )}
            </tbody>
            </table>
        `
    }



    deleteEmpleados(ev, idEmpleado){
        console.log("deleteEmpleados en report-listado");

        console.log("Este es el id empleado: " + idEmpleado);

        this.dispatchEvent(
            new CustomEvent(
                "delete-empleados",
                {
                    detail: {
                        id: idEmpleado
                    }
                }
            )
        )

    }

    infoEmpleado(ev, empleado){
        console.log("infoPerson en report-listado");
        console.log(empleado);


        this.dispatchEvent(
            new CustomEvent(
                "info-empleados-event",
                {
                    detail: {
                      empleado: empleado
                    }
                }
            )
        )
    }

    addEmpleado() {
        console.log("addEmpleado en report-listado");


        this.dispatchEvent(
            new CustomEvent(
                "add-empleado-event",
                {
                    detail: {}
                }
            )
        )
    }

    updated(changedProperties){
        console.log("Entramos en updated de report-listado");

        if(changedProperties.has("empleados")){
            console.log("Cambio la propiedad empleados de report-listado");
            console.log(this.empleados);
            this.empleadosListados = this.empleados;
            console.log(this.empleadosListados);
        }
    }


}
customElements.define('report-listado', ReportListado);


