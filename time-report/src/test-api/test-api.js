
import { LitElement, html } from 'lit';

class TestApi extends LitElement{

    static get properties(){
        return{
            textTest: {type: String}
        };
    }

    constructor(){
        super();

        this.textTest = "";
        this.getTestData();
    }

    render(){
        return html`
            <div>Hello API: ${this.textTest}</div>
        `
    }

    getTestData(){
        console.log("getTestData");
        console.log("Obteniendo datos del test");

        let xhr = new XMLHttpRequest();

        //La ejecucion es asincrona
        xhr.onload = () => {
            //Por defecto es un 200 si ha ido bien
            if(xhr.status === 200){
                console.log("Petición completada con éxito");

                let APIResponse = xhr.responseText;

                this.textTest = APIResponse;
                this.dispatchEvent(
                    new CustomEvent(
                        "test-api-event",
                        {
                            detail: {
                                textTest: this.textTest
                            }
                        }
                    )
                );
            }
        }

        xhr.open("GET", "http://localhost:8080/hello");
        xhr.send();

        console.log("Fin de getTestData");
    }
}

customElements.define("test-api",TestApi);