
import { LitElement, html } from 'lit';

class ReportSearchDM extends LitElement{

    static get properties(){
        return{
            empleados: {type: Array},
            ejecutarSearch: {type: Boolean},
            coduser: {type: String}
        };
    }

    constructor(){
        super();

        this.empleados = [];
        this.coduser = "";
        this.ejecutarSearch = false;
    }

    updated(changedProperties) {
        console.log("updated en report-search-dm");
        if(changedProperties.has("ejecutarSearch")) {
            console.log("Ha cambiado el valor de la propiedad ejecutarSearch en report-search-dm");
            console.log("this.ejecutarSearch: " + this.ejecutarSearch);
            if(this.ejecutarSearch === true) {
                this.getReportData();
            }
        }
    }

    getReportData(){
        console.log("getReportData");
        console.log("Obteniendo datos de los empleados");

        let xhr = new XMLHttpRequest();
        let searchURL = "http://localhost:8080/reports/reports/" + this.coduser;

        console.log(searchURL);

        //La ejecucion es asincrona
        xhr.onload = () => {
            //Por defecto es un 200 si ha ido bien
            if(xhr.status === 200){
                console.log("Petición completada con éxito");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.empleados = APIResponse;

                this.dispatchEvent(
                    new CustomEvent(
                        "report-search-event",
                        {
                            detail: {
                                empleados: this.empleados
                            }
                        }
                    )
                );
            }
        }
        /*console.log("Petición mock");
        const empleados = [{
            id: "1",
            coduser: "213456A",
            nombre: "Blas Cantó",
            tarea: "Control de horas",
            horas: "8"
        }];

        this.dispatchEvent(
            new CustomEvent(
                "report-search-event",
                {
                    detail: {
                        empleados: empleados
                    }
                }
            )
        );*/


        xhr.open("GET", searchURL);
        xhr.send();

        console.log("Fin de getTestData");
    }
}

customElements.define("report-search-dm",ReportSearchDM);