
import { LitElement, html } from 'lit';

class ReportAltaDM extends LitElement{

    static get properties(){
        return{
            empleado: {type: Object},
            ejecutarAlta: {type: Boolean},
        };
    }

    constructor(){
        super();

        this.empleado = {};
        this.ejecutarAlta = false;
    }

    updated(changedProperties) {
        console.log("updated en report-alta-dm");
        if(changedProperties.has("ejecutarAlta")) {
            console.log("Ha cambiado el valor de la propiedad ejecutarAlta en report-alta-dm");
            console.log("this.ejecutarAlta: " + this.ejecutarAlta);
            if(this.ejecutarAlta === true) {
                this.getReportData();
            }
        }
    }

    getReportData(){
        console.log("getReportData");
        console.log("Enviando datos de los empleados");
        console.log(this.empleado);

        let xhr = new XMLHttpRequest();
        let altaURL = "http://localhost:8080/reports/reports"; 

        console.log(altaURL);

        //La ejecucion es asincrona
        xhr.onload = () => {
            //Por defecto es un 200 si ha ido bien
            if(xhr.status === 201){
                console.log("Petición completada con éxito");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.empleados = APIResponse;

                this.dispatchEvent(
                    new CustomEvent(
                        "report-alta-event",
                        {
                            detail: {}
                        }
                    )
                );
            }
        }
        // console.log("Petición mock");
        // const empleados = [{
        //     id: "1",
        //     coduser: "213456A",
        //     nombre: "Blas Cantó",
        //     tarea: "Control de horas",
        //     horas: "8"
        // }];

        // this.dispatchEvent(
        //     new CustomEvent(
        //         "report-search-event",
        //         {
        //             detail: {
        //                 empleados: empleados
        //             }
        //         }
        //     )
        // );

        xhr.open("POST", altaURL);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(this.empleado));

        console.log("Fin de getTestData");
    }
}

customElements.define("report-alta-dm",ReportAltaDM);