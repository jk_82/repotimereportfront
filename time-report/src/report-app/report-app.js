import { LitElement, html } from "lit";
import "../report-header/report-header.js";
import "../report-filter/report-filter.js";
import "../report-listado/report-listado.js";
import "../report-search-dm/report-search-dm.js";
import "../report-delete-dm/report-delete-dm.js";
import "../report-form/report-form.js";
import "../report-alta-dm/report-alta-dm.js";
import "../report-modificar-dm/report-modificar-dm.js";

class ReportApp extends LitElement{

    static get properties(){
        return{
            titleReport: {type: String},
            hiddenForm: {type: Boolean},
            hiddenListado: {type: Boolean},
            hiddenFilter: {type: Boolean},
            modo: {type: String}

        };
    }

    constructor(){
        super();
        this.titleReport = "Búsqueda de tareas/horas";
        this.hiddenForm = true;
        this.hiddenListado=true;
        this.hiddenFilter=false;
        this.modo = "";
    }

    render() {
        return html `
            <report-header titleReport="${this.titleReport}"></report-header>
            <report-filter
                @search-filter-event="${this.searchFilterEvent}"
                ?hidden="${this.hiddenFilter}"></report-filter>
            <report-listado
                @delete-empleados="${this.deleteEmpleado}"
                @add-empleado-event="${this.addEmpleado}"
                @info-empleados-event="${this.infoEmpleado}"
                ?hidden="${this.hiddenListado}"></report-listado>
            <report-form
                ?hidden="${this.hiddenForm}"
                @form-close-event="${this.goBack}"
                @report-add-empleado="${this.guardarEmpleado}"></report-form>
            <report-search-dm @report-search-event="${this.responseSearch}"></report-search-dm>
            <report-delete-dm @report-delete-event="${this.responseDelete}"></report-delete-dm>
            <report-alta-dm @report-alta-event="${this.reponseAlta}"></report-alta-dm>
            <report-modificar-dm @report-modificar-event="${this.responseModificar}"></report-modificar-dm>
        `
    }

    responseSearch(ev) {
        console.log("responseSearch en report-app");
        this.shadowRoot.querySelector("report-listado").empleados = ev.detail.empleados;
        console.log(ev.detail.empleados);
        this.hiddenForm = true;
        this.hiddenListado = false;
        this.hiddenFilter = false;
        this.shadowRoot.querySelector("report-search-dm").ejecutarSearch = false;
    }
    searchFilterEvent(ev) {
        console.log("searchFilterEvent en report-app");
        console.log(ev.detail);
        this.shadowRoot.querySelector("report-search-dm").coduser = ev.detail.coduser;
        this.shadowRoot.querySelector("report-search-dm").ejecutarSearch = true;
    }


    deleteEmpleado(ev) {
        console.log("deleteEmpleado en report-app");
        console.log(ev.detail.id);
        this.shadowRoot.querySelector("report-delete-dm").empleadoId = ev.detail.id;
        this.shadowRoot.querySelector("report-delete-dm").ejecutarDelete = true;
        this.shadowRoot.querySelector("report-search-dm").ejecutarSearch = false;
    }
    responseDelete() {
        console.log("responseDelete en report-app");
        this.shadowRoot.querySelector("report-search-dm").ejecutarSearch = true;
        this.shadowRoot.querySelector("report-delete-dm").ejecutarDelete = false;
    }

    addEmpleado(){
        console.log("addEmpleado en report-app");

        this.modo = "A";
        this.hiddenForm = false;
        this.hiddenListado = true;
        this.hiddenFilter = true;
        this.shadowRoot.querySelector("report-header").titleReport = "Alta de horas";
    }
    guardarEmpleado(ev){
        console.log("guardarEmpleado en report-app");
        console.log(ev.detail);

        if(this.modo === "A"){
            this.shadowRoot.querySelector("report-alta-dm").empleado = ev.detail.empleado;
            this.shadowRoot.querySelector("report-alta-dm").ejecutarAlta = true;
        }else{
            this.shadowRoot.querySelector("report-modificar-dm").empleado = ev.detail.empleado;
            this.shadowRoot.querySelector("report-modificar-dm").ejecutarModificar = true;
        }


    }
    reponseAlta(){
        console.log("reponseAlta en report-app");
        this.shadowRoot.querySelector("report-search-dm").ejecutarSearch = true;
        this.shadowRoot.querySelector("report-alta-dm").ejecutarAlta = false;

        this.hiddenForm = true;
        this.hiddenListado = false;
        this.hiddenFilter = false;
    }

    infoEmpleado(ev) {
        console.log("infoEmpleado en report-app");
        console.log(ev.detail);

        this.modo = "M";
        this.hiddenForm = false;
        this.hiddenListado = true;
        this.hiddenFilter = true;
        this.shadowRoot.querySelector("report-header").titleReport = "Modificación de empleados";
        this.shadowRoot.querySelector("report-form").empleado = ev.detail.empleado;
    }
    responseModificar(){
        console.log("responseModificar en report-app");
        this.shadowRoot.querySelector("report-search-dm").ejecutarSearch = true;
        this.shadowRoot.querySelector("report-modificar-dm").ejecutarModificar = false;

        this.hiddenForm = true;
        this.hiddenListado = false;
        this.hiddenFilter = false;
    }

    goBack() {
        console.log("goBack en report-app");

        this.hiddenForm = true;
        this.hiddenListado = false;
        this.hiddenFilter = false;
        this.shadowRoot.querySelector("report-header").titleReport = "Búsqueda de tareas/horas";

    }

}
customElements.define('report-app', ReportApp);