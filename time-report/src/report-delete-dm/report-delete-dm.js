
import { LitElement, html } from 'lit';

class ReportDeleteDM extends LitElement{

    static get properties(){
        return{
            ejecutarDelete: {type: Boolean},
            empleadoId: {type: String}
        };
    }

    constructor(){
        super();
        this.ejecutarDelete = false;
        this.empleadoId = "";
    }

    updated(changedProperties) {
        console.log("updated en report-delete-dm");
        if(changedProperties.has("ejecutarDelete")) {
            console.log("id: " + this.empleadoId);
            console.log("Ha cambiado el valor de la propiedad ejecutarDelete en report-delete-dm");
            console.log("this.ejecutarDelete: " + this.ejecutarDelete);
            if(this.ejecutarDelete === true) {
                this.getReportData();
            }
        }
    }

    getReportData(){
        console.log("getReportData en delete");

        let xhr = new XMLHttpRequest();
        let deleteURL = "http://localhost:8080/reports/reports/" + this.empleadoId;

        //La ejecucion es asincrona
        xhr.onload = () => {
            //Por defecto es un 200 si ha ido bien
            if(xhr.status === 200){
                console.log("Petición completada con éxito");

                this.dispatchEvent(
                    new CustomEvent(
                        "report-delete-event",
                        {
                            detail: {}
                        }
                    )
                );
            }
        }

        console.log(deleteURL);

         xhr.open("DELETE", deleteURL);
         xhr.send();

        console.log("Fin de getTestData");
    }
}

customElements.define("report-delete-dm",ReportDeleteDM);