import { LitElement, html } from "lit";


class ReportFilter extends LitElement{

    static get properties(){
        return{
            coduser: {type: String}
        };
    }

    constructor(){
        super();
        this.coduser = "";

    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Código de empleado</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Código de empleado"
                            .value="${this.coduser}"
                            @input="${this.updateCodigoEmpleado}"
                        >
                        </input>
                    </div>
                    <button class="btn btn-primary" 
                    style="margin-top: 2px;"
                    @click="${this.search}"><strong>Buscar</strong></button>
                </form>
            </div>
        `
    }

    updateCodigoEmpleado(ev) {
        console.log("updateCodigoEmpleado en report-filter");
        this.coduser = ev.target.value;
    }

    search(ev) {
        ev.preventDefault();
        console.log("search en report-filter");
        this.dispatchEvent(
            new CustomEvent(
                "search-filter-event",
                {
                    detail: {
                        coduser: this.coduser
                    }
                }
            )
        );
    }
}
customElements.define('report-filter', ReportFilter);