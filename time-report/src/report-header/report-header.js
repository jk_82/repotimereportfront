import { LitElement, html } from "lit";


class ReportHeader extends LitElement{

    static get properties(){
        return{
            titleReport: {type: String}
        };
    }

    constructor(){
        super();
        this.titleReport = "";

    }

    render() {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div class="text-center">
            <h2>${this.titleReport}</h2>
        </div>
        `
    }

}
customElements.define('report-header', ReportHeader);