
import { LitElement, html } from 'lit';

class ReportModificarDM extends LitElement{

    static get properties(){
        return{
            empleado: {type: Object},
            ejecutarModificar: {type: Boolean},
        };
    }

    constructor(){
        super();

        this.empleado = {};
        this.ejecutarModificar = false;
    }

    updated(changedProperties) {
        console.log("updated en report-modificar-dm");
        if(changedProperties.has("ejecutarModificar")) {
            console.log("Ha cambiado el valor de la propiedad ejecutarModificar en report-alta-dm");
            console.log("this.ejecutarModificar: " + this.ejecutarModificar);
            if(this.ejecutarModificar === true) {
                this.getReportData();
            }
        }
    }

    getReportData(){
        console.log("getReportData");
        console.log("Enviando datos de los empleados");
        console.log(this.empleado);

        let xhr = new XMLHttpRequest();
        let modificarURL = "http://localhost:8080/reports/reports/"+this.empleado.id;

        console.log(modificarURL);

        //La ejecucion es asincrona
        xhr.onload = () => {
            //Por defecto es un 200 si ha ido bien
            if(xhr.status === 200){
                console.log("Petición completada con éxito");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.empleados = APIResponse;

                this.dispatchEvent(
                    new CustomEvent(
                        "report-modificar-event",
                        {
                            detail: {}
                        }
                    )
                );
            }
        }
        // console.log("Petición mock");
        // const empleados = [{
        //     id: "1",
        //     coduser: "213456A",
        //     nombre: "Blas Cantó",
        //     tarea: "Control de horas",
        //     horas: "8"
        // }];

        // this.dispatchEvent(
        //     new CustomEvent(
        //         "report-search-event",
        //         {
        //             detail: {
        //                 empleados: empleados
        //             }
        //         }
        //     )
        // );
        xhr.open("PUT", modificarURL);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(this.empleado));

        console.log("Fin de modificar");
    }
}

customElements.define("report-modificar-dm",ReportModificarDM);