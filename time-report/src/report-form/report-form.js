import { LitElement, html } from "lit";


class ReportForm extends LitElement{

    static get properties(){
        return{
            empleado: {type: Object}
        };
    }

    updated(changedProperties){
        console.log("Entramos en updated de report-form");
        this.camposRellenos();
    }

    constructor(){
        super();
        this.empleado = {};
        this.resetFormData();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div>
                <form>
                <div class="form-group">
                        <label>Codigo de usuario</label>
                        <input class="form-control" type="text"
                        placeholder="Codigo usuario" @input="${this.updateCodigo}"
                        .value="${this.empleado.coduser}"/>
                    </div>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input class="form-control" type="text"
                        placeholder="Nombre completo" @input="${this.updateName}"
                        .value="${this.empleado.nombre}"/>
                    </div>
                    <div class="form-group">
                        <label>Tarea</label>
                        <textarea class="form-control" type="text"
                        placeholder="Tarea" rows="5"
                        @input="${this.updateTarea}"
                        .value="${this.empleado.tarea}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Horas</label>
                        <input class="form-control" type="number"
                        @input="${this.updateHoras}"
                        placeholder="Horas"
                        .value="${this.empleado.horas}"/>
                    </div>
                    <button class="btn btn-primary"
                    @click="${this.goBack}">
                        <strong>Atrás</strong>
                    </button>
                    <button id="btnGuardar" class="btn btn-success"
                    @click="${this.guardarEmpleado}">
                        <strong>Guardar</strong>
                    </button>
                </form>
            </div>
        `
    }

    goBack(ev){
        console.log("goBack");
        console.log("Se ha pulsado btn atras form persona");


        ev.preventDefault();
        this.dispatchEvent(new CustomEvent("form-close-event", {}));
        this.resetFormData();
    }

    updateCodigo(ev) {
        console.log("updateCodigo");
        console.log("Estoy guardando en la propiedad coduser "+ ev.target.value);

        this.empleado.coduser = ev.target.value;
        this.camposRellenos();
    }

    updateName(ev) {
        console.log("updateName");
        console.log("Estoy guardando en la propiedad nombre "+ ev.target.value);

        this.empleado.nombre = ev.target.value;
        this.camposRellenos();
    }
    updateTarea(ev) {
        console.log("updateTarea");
        console.log("Estoy guardando en la propiedad tarea "+ ev.target.value);

        this.empleado.tarea = ev.target.value;
        this.camposRellenos();
    }

    updateHoras(ev) {
        console.log("updateHoras");
        console.log("Estoy guardando en la propiedad horas "+ ev.target.value);

        this.empleado.horas = parseInt(ev.target.value);
        this.camposRellenos();
    }

    camposRellenos() {
        console.log("camposRellenos report-form");
        console.log(this.empleado);
        console.log(this.empleado.horas);
        if(this.empleado.coduser !== "" && this.empleado.coduser !== null &&
            this.empleado.nombre !== "" && this.empleado.nombre !== null &&
            this.empleado.tarea !== "" && this.empleado.tarea !== null &&
            this.empleado.horas > 0 && this.empleado.horas !== null) {
                console.log("camposRellenos en if");
                this.shadowRoot.querySelector("#btnGuardar").disabled = false;
        } else {
            this.shadowRoot.querySelector("#btnGuardar").disabled = true;
        }
    }
    guardarEmpleado(ev) {
        console.log("storeRobin");
        console.log(ev);
        ev.preventDefault();

        console.log("La propiedad coduser vale: " + this.empleado.coduser);
        console.log("La propiedad nombre vale: " + this.empleado.nombre);
        console.log("La propiedad tarea vale: " + this.empleado.tarea);
        console.log("La propiedad horas vale: " + this.empleado.horas);

        this.dispatchEvent(
            new CustomEvent(
                "report-add-empleado",
                {
                    detail: {
                        empleado: {
                            coduser: this.empleado.coduser,
                            nombre: this.empleado.nombre,
                            tarea: this.empleado.tarea,
                            horas: this.empleado.horas,
                            id: this.empleado.id,
                        }
                    }
                }
            )
        )
    }


    resetFormData(){
        this.empleado = {};

        this.empleado.coduser = "";
        this.empleado.nombre = "";
        this.empleado.tarea = "";
        this.empleado.horas = NaN;
    }
}
customElements.define('report-form', ReportForm);